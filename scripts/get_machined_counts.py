"""
===================
Label image regions
===================
http://scikit-image.org/docs/dev/auto_examples/segmentation/plot_label.html#sphx-glr-download-auto-examples-segmentation-plot-label-py

This example shows how to segment an image with image labelling. The following
steps are applied:

1. Thresholding with automatic yen method
2. Close small holes with binary closing
3. Measure image regions to filter small objects


TODO:
 investigate regions
 iterate through images
 check with labels

"""

import os, csv
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import numpy as np

from skimage import data, io
from skimage.util import img_as_ubyte
from skimage.filters import threshold_yen, threshold_isodata, threshold_otsu
from skimage.segmentation import clear_border
from skimage.measure import label, regionprops
from skimage.color import label2rgb, rgb2gray
from PIL import Image

from skimage.morphology import erosion, dilation, opening, closing, white_tophat
from skimage.morphology import black_tophat, skeletonize, convex_hull_image
from skimage.morphology import disk, square

def get_labelled_image(image):
    # apply threshold
    thresh = threshold_yen(image)
    bw = closing(image > thresh, square(3))
    # label image regions
    return label(bw)

def count_regions(image):
    """ returns the number of discovered regions in the image """
    labelled_image = get_labelled_image(image)

    count = 0
    for region in regionprops(labelled_image):
        # take regions with large enough areas
        if region.area >= 1200:
            # and perform a second pass
            count += len(get_cropped_regions(image, region.bbox))
        elif region.area >= 200:
            count += 1

    return count

def crop_image(image, coords):
    """ given an image and coordinates forming a rectangle within the image
        return a cropped subimage.  images are numpy arrays """
    minr, minc, maxr, maxc = coords
    return image[minr:maxr+1, minc:maxc+1]

def get_cropped_regions(image, coords):
    """ given an image and a bounding box, crop the image, and do specific close-up
        filtering to separate cells. """
    
    # crop and filter the image
    cropped = crop_image(image, coords)
    cropped = erosion(cropped)
    thresh = threshold_yen(cropped)
    binary_cropped = opening(closing(cropped > thresh, square(3)), square(3))
    labelled_cropped = label(binary_cropped)

    # return the number of regions discovered with an area greater than 200 pixels
    regions = filter(lambda r: r.area >= 200, regionprops(labelled_cropped))
    return list(regions)

def label_regions(image):
    """ display an image with an overlay displaying detected regions """
    # creating filtered versions of the image for later use and display
    labelled_image = get_labelled_image(image)
    image_label_overlay = rgb2gray(label2rgb(labelled_image, image=image))

    # initializing the plots and figures
    fig, ax = plt.subplots(figsize=(10, 6))
    ax.imshow(image_label_overlay)

    # documentation on regionprops: http://scikit-image.org/docs/dev/api/skimage.measure.html#skimage.measure.regionprops
    # get the regions from the labelled image. properties of those regions are 
    # documented in the link above. we iterate through each region, and draw the bounding box 
    for region in regionprops(labelled_image):
        if region.area >= 200:                                                  # smaller regions are likely noise
            clr = 'green' if region.area < 1200 else 'red'                      # larger regions likely have multiple cells
            minr, minc, maxr, maxc = region.bbox
            rect = mpatches.Rectangle((minc, minr), maxc - minc, maxr - minr,   # get the bounding box and draw it on our image display
                                      fill=False, edgecolor=clr, linewidth=1)
            ax.add_patch(rect)
            if region.area >= 1200:
                for lilregion in get_cropped_regions(image, region.bbox):       # execute further fultering and processing
                    print('drawing lilregion. area={}'.format(lilregion.area))  # on larger regions
                    bigminr, bigminc, _, _ = region.bbox
                    minr, minc, maxr, maxc = lilregion.bbox
                    rect = mpatches.Rectangle((bigminc + minc, bigminr + minr), 
                                                maxc - minc, maxr - minr,
                                                fill=False, edgecolor='yellow', 
                                                linewidth=1)
                    ax.add_patch(rect)

    print(count_regions(image) + " bacterial cells")
    ax.set_axis_off()
    plt.tight_layout()
    plt.show()              # display image

def write_counts(counts, fn="machined_counts.csv"):
    """ given counts, a list of tuples (filename, count),
        write data to a csv file. """
    with open("tables/" + fn, 'w') as out:
        csv_out=csv.writer(out)
        csv_out.writerow(['filename', 'count'])
        csv_out.writerows(counts)

def plot_comparison(original, filtered, filter_name):
    """ given two images, display them both to accent the differences. """
    fig, (ax1, ax2) = plt.subplots(ncols=2, figsize=(8, 4), sharex=True,
                                   sharey=True)
    ax1.imshow(original, cmap=plt.cm.gray)
    ax1.set_title('original')
    ax1.axis('off')
    ax2.imshow(filtered, cmap=plt.cm.gray)
    ax2.set_title(filter_name)
    ax2.axis('off')
    plt.show()

def main():
    """
    Prompt for user input whether they would like the program to crunch all the
    counts, or if they would like to look at each individual image
    """

    data_dir = "./images/"

    choice = input("Run counts on all images? (y/n)? ")

    # gets counts from all images and stores them to the csv (default fn="machined_counts.csv")
    if choice == "y":
        counts = []
        nFiles = len(os.listdir(data_dir))
        for i, fn in enumerate(os.listdir(data_dir)):
            # progress bar
            print(" {:02.2f}% of image regions counted... ({})"                 
                    .format(100*i/nFiles, fn), end="\r")
                    
            if fn[-3:] == "jpg":
                path = os.path.join(data_dir, fn)
                
                # load the image
                image = io.imread(path, as_gray=True)    
                
                # get count for each image
                img_count = count_regions(image)
                counts.append((fn, img_count))

        # write to csv file
        write_counts(counts)

    else:
        fn = str(input("Enter filename keycode: ")) + ".jpg"
        path = os.path.join(data_dir, fn)
        image = io.imread(path, as_gray=True)
        print("labelling regions...")
        # display image and the discovered regions
        label_regions(image)

main()

import csv

def get_file_map():
    """ read the image filenames csv file, and return a dictionary where
        (number filename) => original
    """
    name_map = {}
    with open("tables/image_names.csv") as names:
        reader = csv.reader(names)
        next(reader)
        next(reader)
        for row in reader:
            name_map.update({"{:02d}.jpg".format(int(row[1])): row[0]})
    return name_map

def read_CSV(filename):
    with open("tables/" + filename) as csvfile:
        reader = csv.DictReader(csvfile)
        return [row for row in reader]

def write_CSV(filename, rows):
    with open("tables/" + filename, 'w') as csvfile:
        writer = csv.DictWriter(csvfile, rows[0].keys())
        writer.writeheader()
        writer.writerows(rows)

def get_counts(counts_list, image_name):
    """ given a list of counts, and an image to search for,
        return the row associated with imageName """
    for row in counts_list:
        if row["Image"] == image_name:
            return row
    print("ERROR: could not find image: " + image_name)

def main():
    # read all 3 csv files (human/machine counts and filename key)
    name_map = get_file_map()
    machined_counts = read_CSV("machined_counts.csv")
    human_counts = read_CSV("human_counts.csv")
    counts = []

    # create 3 new columns for compared values
    sumrow = {'delta':0, '% delta': 0}
    for row in machined_counts:
        newrow = get_counts(human_counts, name_map[row["filename"]])
        newrow['alt_filename'] = row["filename"]
        newrow['machined_count'] = row["count"]
        newrow['delta'] = float(newrow["Type 1 Avg"]) - float(newrow["machined_count"])
        newrow['% delta'] = newrow['delta'] / float(newrow["Type 1 Avg"])
        counts.append(newrow)

        sumrow['delta'] += newrow['delta']
        sumrow['% delta'] += newrow['% delta']

    avgrow = {'alt_filename': "averages",
              'delta': sumrow['delta']/len(machined_counts),
              '% delta': sumrow['% delta']/len(machined_counts)}
    counts.append(avgrow)
    write_CSV("total_counts.csv", counts)

if __name__ == "__main__":
    main()
